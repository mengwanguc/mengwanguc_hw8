## README ###
NAME: Meng Wang
This is my assignment for homework 8.


### 8.1 ###
The 8.1 folder contains my source code for problem 8.1.


### 8.2 ###
The 8.2 folder contains my source codes for problem 8.2.

#### Execution time test: #####
##### Matrix: #####
Original code: 1.566 sec
Modified code: 0.672 sec

##### PSMatrix: #####
Original code: 1.649 sec
Modified code: 0.713 sec

##### OverloadMatrix: #####
Original code: 1.714 sec
Modified code: 0.824 sec

We can see all of them show better performance after modification.

### 8.4 ###
8.4 folder contains my source code for problem 8.4.

### 8.5 ###
8.5 folder contains my source code for problem 8.5.