#include "OverloadMatrix.h"
#include <iostream>
using namespace std;
using namespace mpcs51044;

int main()
{
	Matrix<double, 2, 2> m = { 
			{ 1, 2, }, 
			{ 3, 4, }
	};
	static double total = 0;
	double begin = clock();
	for (int i = 0; i < 10000000; i++) {
		m(1, 1) = i;
		total += m.determinant();
	}
	double end = clock();
	cout << "total: " << total << endl;
	cout << "time: " << (end - begin) / CLOCKS_PER_SEC << " sec" << endl;
}