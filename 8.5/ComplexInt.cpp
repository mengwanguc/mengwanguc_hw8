#include <iostream>

using namespace std;

class ComplexInt {
public:
	ComplexInt(int r, int i) : r(r), i(i) {}
	
	ComplexInt(int r) : r(r), i(0) {}

	int real() {	return r;}
	int imag() {	return i;}
	
	// overload +
	ComplexInt operator + (const ComplexInt &c2) const {
		return ComplexInt(r + c2.r, i + c2.i);
	}

	// overload -
	ComplexInt operator - (const ComplexInt &c2) const {
		return ComplexInt(r - c2.r, i - c2.i);
	}
	
	// overload *
	ComplexInt operator * (const ComplexInt &c2) const {
		return ComplexInt(r * c2.r - i * c2.i, r * c2.i + i * c2.r);
	}
	
	// overload <<
	inline friend ostream &operator << (ostream &os, const ComplexInt &c) {
		// if c == 0, then print 0 directory
		if (c.r == 0 && c.i == 0) {
			os << "0";
			return os;
		}

		// print the real part
		// if r == 0, don't print the real part
		if (c.r != 0) {
			os << c.r;
		}

		// if i > 0, print "+"
		if (c.i > 0) {
			os << "+";
		}

		// print the imaginary part
		// if i == 0, don't print the imaginary part
		if (c.i != 0) {
			os << c.i << "i";
		}

		return os;
	}


private:
	int r;		// real part
	int i;		// imaginary part
};


// user defined literals
ComplexInt operator"" _i(unsigned long long y) {
	ComplexInt res(0, y);
	return res;
}

// int + ComplexInt
ComplexInt operator + (const int &r, const ComplexInt &i) {
	return ComplexInt(r, 0) + i;
}

// int + ComplexInt
ComplexInt operator - (const int &r, const ComplexInt &i) {
	return ComplexInt(r, 0) - i;
}

int main() {
	ComplexInt ci = 6 + 3_i;
	ComplexInt ci2 = 6 - 3_i;
	cout << "ci: " << ci << endl;
	cout << "ci2: " << ci2 << endl;
}