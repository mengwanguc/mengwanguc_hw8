#include <iostream>

using namespace std;

class ComplexInt {
public:
	ComplexInt(int r, int i) : r(r), i(i) {}

	// overload +
	ComplexInt operator + (const ComplexInt &c2) const {
		return ComplexInt(r + c2.r, i + c2.i);
	}

	// overload -
	ComplexInt operator - (const ComplexInt &c2) const {
		return ComplexInt(r - c2.r, i - c2.i);
	}
	
	// overload *
	ComplexInt operator * (const ComplexInt &c2) const {
		return ComplexInt(r * c2.r - i * c2.i, r * c2.i + i * c2.r);
	}
	
	// overload <<
	inline friend ostream &operator << (ostream &os, const ComplexInt &c) {
		// if c == 0, then print 0 directory
		if (c.r == 0 && c.i == 0) {
			os << "0";
			return os;
		}

		// print the real part
		// if r == 0, don't print the real part
		if (c.r != 0) {
			os << c.r;
		}

		// if i > 0, print "+"
		if (c.i > 0) {
			os << "+";
		}

		// print the imaginary part
		// if i == 0, don't print the imaginary part
		if (c.i != 0) {
			os << c.i << "i";
		}

		return os;
	}


private:
	int r;		// real part
	int i;		// imaginary part
};


int main() {
	ComplexInt c1(1, 2);
	ComplexInt c2(3, 4);
	cout << "c1: " << c1 << endl;
	cout << "c2: " << c2 << endl;
	cout << "c1 + c2: " << c1 + c2 << endl;
	cout << "c1 - c2: " << c1 - c2 << endl;
	cout << "c1 * c2: " << c1 * c2 << endl;
}