#include "Matrix.h"
#include <iostream>
using namespace mpcs51044;
using namespace std;

int main()
{
	Matrix<3, 3> m1 = { 
			{ 1, 2, 3, }, 
			{ 4, 5, 6, }, 
			{ 7, 8, 9, } 
	};

	Matrix<3, 3> m2 = {
		{ 1, 2, 3, },
		{ 4, 5, 6, },
		{ 7, 8, 9, }
	};

	// test
	// add m1 and m2 and print
	cout << m1 + m2 << endl;
	
}